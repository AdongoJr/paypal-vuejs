declare module '*.vue' {
  import Vue from 'vue'

  declare global {
    // enabling paypal globally
    interface Window {
      paypal: any;
    }
  }

  export default Vue
}
